import numpy as np
import scipy.interpolate
from . import quats as Q


def residual_weights(residuals, scale=None):
    #return 1.0/np.maximum(residuals, 1e-6)
    T = 1.345
    if scale is None:
        scale = np.median(np.fabs(residuals - np.median(residuals)))
    scale = scale/0.6745
    r = residuals/scale
    return 1.0/(1.0 + r)
    weights = np.ones(len(r))
    outliers = r > T
    weights[outliers] = T/r[outliers]
    return weights


def spline_smoother(t, x, w=None, std=0.1):
    xtype = type(x)
    if w is None:
        w = np.zeros(len(t)) + 1.0/std
    t0 = t[0]
    dur = t[-1] - t0
    u = (t - t0)/dur
    spl = scipy.interpolate.splprep(x.T, u=u, w=w, s=len(w)/3.0, nest=len(t)+3+1)
    def interp(t):
        u = (t - t0)/dur
        ret = scipy.interpolate.splev(u, spl[0])
        return np.array(ret).T.copy().view(xtype)
    return interp

def robust_spline(t, x, std=0.1, max_iters=50, tol=1e-6):
    t = np.asarray(t)
    x = np.asarray(x)
    weights = np.ones(len(t))/std
    os = np.sum(weights)
    prev_errors = np.zeros(len(weights)) + np.inf
    for i in range(max_iters):
        fit = spline_smoother(t, x, weights, std)
        fitted = fit(t)
        errors = np.sum((x - fitted)**2, axis=1)
        weights = residual_weights(errors, std)
        weights /= np.sum(weights)
        weights *= os
        if np.max(np.fabs(prev_errors - errors)) < tol:
            break
        prev_errors = errors
    return fit

def qspline(t, rots, weights=None, angle_std=0.1):
    interp = spline_smoother(t, rots, w=weights, std=angle_std)
    return lambda nt: Q.normalize(interp(nt))

def robust_qspline(t, rots, angle_std=0.1, tol=1e-6, max_iters=50):
    t = np.asarray(t)
    weights = np.ones(len(t))/angle_std
    os = np.sum(weights)
    prev_errors = np.zeros(len(weights)) + np.inf
    for i in range(max_iters):
        #print "Q", i
        fit = qspline(t, rots, weights, angle_std)
        fitted = fit(t)
        #errors = Q.angle(fitted, rots)
        errors = Q.rownorm(rots - fitted).reshape(-1)
        weights = residual_weights(errors, angle_std)
        weights /= np.sum(weights)
        weights *= os
        if np.max(np.fabs(prev_errors - errors)) < tol:
            break
        prev_errors = errors
    return fit

def smooth_pose(ts, quats, tvecs, tstd=0.005, rstd=0.005):
    rinterp = robust_qspline(ts, sanify_quats(quats), rstd)
    tinterp = robust_spline(ts, tvecs, tstd)
    def interp(t):
        return rinterp(t), tinterp(t)

    return interp

# Avoid interpolating the "long way around"
def sanify_quats(quats):
    orig = quats
    quats = orig.copy()
    
    for i in range(1, len(quats)):
        if np.dot(quats[i-1], quats[i]) < 0.0:
            quats[i] *= -1
    return quats

def interpolate_pose(ts, quats, tvecs):
    rinterp = scipy.interpolate.interp1d(ts, sanify_quats(quats), axis=0)
    tinterp = scipy.interpolate.interp1d(ts, tvecs, axis=0)
    def interp(t):
        return Q.normalize(rinterp(t)), tinterp(t)
    return interp

def test():
    import matplotlib.pyplot as plt
    #np.random.seed(0)
    vec = [1.0, 0.0, 0.0]

    axis = [0.0, 0.0, -1.0]
    ts = np.linspace(0, np.pi*6, 6000*10)
    angles = (ts*2).reshape(-1, 1).copy()
    #angles = np.zeros(len(ts))
    angles[len(angles)//2:] = np.pi/2.0
    aa = axis*angles.reshape(-1, 1)
    naa = aa.copy()
    noise_level = 0.01
    naa[:,-1] += np.random.randn(len(naa))*noise_level
    outliers = np.random.choice(len(naa), int(len(naa)*0.1))
    naa[outliers,-1] += np.random.randn(len(outliers))*np.pi/4.0

    oquats = Q.from_rvec(aa)
    #sample = np.linspace(0, len(ts)-1, 80).astype(np.int)
    sample = slice(0, None)
    plt.plot(ts, oquats[:,0])
    quats = Q.from_rvec(naa)[sample]


    plt.plot(ts[sample], quats[:,0], '.')
    #for iters in range(1, 20):
    #    rfit = robust_qspline(ts[sample], quats, angle_std=noise_level, max_iters=iters)(ts)
    #    plt.plot(ts, rfit[:,0], '-', label='RQspline')

    rfit = robust_qspline(ts[sample], quats, angle_std=noise_level*3.0)(ts)
    plt.plot(ts, rfit[:,0], '-', label='RQspline')
    #bfit = qspline(ts[sample], quats, angle_std=noise_level)(ts)
    #plt.plot(ts, bfit[:,0], '-', label='Qspline')

    plt.legend()

    plt.show()

if __name__ == '__main__': test()

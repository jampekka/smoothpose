import numpy as np

def rownorm(x):
    return np.sqrt(np.sum(np.asarray(x)**2, axis=1)).reshape(-1, 1)

class Quat(np.ndarray):
    def __new__(cls, *args, **kwargs):
        return np.asarray(*args, **kwargs).view(cls)
    
    @staticmethod
    def from_rvec(rvec):
        rvec = np.asarray(rvec).reshape(-1, 3)
        quat = Quat(np.zeros((len(rvec), 4)))
        angle = quat[:,0:1] = rownorm(rvec)
        with np.errstate(divide='ignore', invalid='ignore'):
            quat[:,1:] = (rvec/angle)*np.sin(angle/2.0)
        quat[~np.isfinite(quat)] = 0.0
        quat[:,0:1] = np.cos(angle/2.0)
        return quat
    
    def to_rvec(quat):
        quat = np.array(quat).reshape(-1, 4)
        angle = np.arccos(quat[:,0:1])*2.0
        rvec = quat[:, 1:].copy()
        with np.errstate(divide='ignore', invalid='ignore'):
            rvec /= rownorm(rvec)
        rvec[~np.isfinite(rvec)] = 0.0
        rvec *= angle
        return rvec
    
    def normalize(quat):
        out = np.asanyarray(quat).copy().reshape(-1, 4)
        with np.errstate(divide='ignore', invalid='ignore'):
            out[:] = out/rownorm(out)
        zeros = ~np.isfinite(np.sum(out, axis=1))
        out[zeros,1:] = 0.0
        out[zeros,0] = 1.0
        return out
    
    def dot(a, b):
        atype = type(a)
        a = np.asarray(a).reshape(-1, 4)
        b = np.asarray(b).reshape(-1, 4)
        out = np.empty((max(len(a), len(b)), 4))
        ra, rb = a[:,0:1], b[:,0:1]
        va, vb = a[:,1:], b[:,1:]
        out[:,0:1] = ra*rb - np.sum(va*vb, axis=1).reshape(-1, 1)
        out[:,1:] = ra*vb + rb*va + np.cross(va, vb)
        return out.view(atype)
    
    def conj(q, out=None):
        qc = q.reshape(-1, 4).copy()
        qc[:,1:] *= -1.0
        return qc
    
    def angle(a, b):
        d = np.sum(np.multiply(a, b), axis=1)
        c = 2.0*d**2 - 1.0
        # There's occasionally a rounding error causing
        # some numbers to be slightly out of arccos' domain.
        np.clip(c, -1.0, 1.0, c)
        return np.arccos(c)

    
    def rotate(q, v):
        q = np.array(q).reshape(-1, 4)
        v = np.array(v).reshape(-1, 3)
        out = np.zeros((max(len(v), len(v)), 4))
        out[:,1:] = v
        out = dot(dot(q, out), conj(q))
        return out[:,1:]

        def mean_rotation(qs):
            # TODO: Should be vectorized
            out = np.outer(qs[0], qs[0].conj())
            for i in range(1, len(qs)):
                out += np.outer(qs[i], qs[i].conj())
            evals, evecs = np.linalg.eig(out)
            return evecs[np.argmax(evals)]

for name, value in Quat.__dict__.items():
    if name.startswith('_'): continue
    if hasattr(value, '__call__'):
        locals()[name] = value.__call__
    elif hasattr(value, '__func__'):
        locals()[name] = value.__func__

def test():
    import quaternion
    
    r1 = np.random.randn(10, 3)
    r1[0] *= 0.0
    q1 = quaternion.as_float_array(quaternion.from_rotation_vector(r1))
    q2 = Quat.from_rvec(r1)
    assert np.allclose(q1, q2)

    r2 = to_rvec(q1)
    assert np.allclose(r1, r2)

    q3 = np.random.randn(10, 4)
    qp1 = quaternion.as_quat_array(q2)*quaternion.as_quat_array(q3)
    qp1 = quaternion.as_float_array(qp1)
    qp2 = dot(q2, q3)
    assert np.allclose(qp1, qp2)
    assert np.allclose(qp1, q2*q3)

    qc1 = quaternion.as_float_array(quaternion.as_quat_array(q3).conj())
    qc2 = conj(q3)
    assert np.allclose(qp1, qp2)
    
if __name__ == '__main__': test()
